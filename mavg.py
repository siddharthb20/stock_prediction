# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 21:05:48 2021

@author: Siddharth
"""

import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np
from data_loader import dataloader

def moving_average():
    
    df = dataloader()
    
    df['Adj Close'].plot()
    
    df['Moving_avg'] = df['Adj Close'].rolling(window = 50, min_periods = 0).mean()
    
    df['Moving_avg'].plot()
    
    volume_change = []
    adj_close_change = []
    
    for i in range(len(df) - 1):
        
        i = i + 1
        volume_change.append(df.iloc[i]['Volume'] - df.iloc[i-1]['Volume'])
        adj_close_change.append(df.iloc[i]['Adj Close'] - df.iloc[i-1]['Adj Close'])
        
    df['Change in volume']=volume_change
    df['Change in Adj Close']=adj_close_change
    
moving_average()