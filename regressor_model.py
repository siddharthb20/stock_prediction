# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 17:14:48 2021

@author: Siddharth
"""

import numpy as np
import pandas as pd
from data_pre_processing import load_data
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Flatten
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import accuracy_score
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
import tensorflow as tf
import matplotlib.pyplot as plt



BATCH_SIZE = 32
EPOCHS = 2000

def regressor_model():
    
    model = Sequential([
                Dense(32, kernel_initializer='normal',input_dim = 200, activation='relu'),
                Dense(64, kernel_initializer='normal',activation='relu'),
                Dense(128, kernel_initializer='normal',activation='relu'),
                Dense(256, kernel_initializer='normal',activation='relu'),
                Dense(4, kernel_initializer='normal',activation='linear')])

    model.summary()
    
    model.compile(loss='mean_absolute_error', optimizer='adam', metrics=['accuracy','mean_absolute_error'])
    
    return model

X, y = load_data()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)



regressor = KerasRegressor(build_fn = regressor_model, batch_size=BATCH_SIZE, epochs=EPOCHS)

callback = tf.keras.callbacks.ModelCheckpoint(filepath='Regressor_model.tf',
                                          monitor='mean_absolute_error',
                                          verbose=0,
                                          save_best_only=True,
                                          save_weights_only=False,
                                          mode='auto')

results = regressor.fit(X_train,y_train,callbacks=[callback])

y_pred = regressor.predict(X_test)
print(y_pred)
y_pred_mod = []
y_test_mod = []

y_test_numpy = y_test.to_numpy()
#print(type(y_pred), type(y_test_numpy))

for i in range(0, 4):
    j = 0
    y_pred_temp = []
    y_test_temp = []
    
    for j in range(len(y_test)):
        y_pred_temp.append(y_pred[j][i])
        y_test_temp.append(y_test_numpy[j][i])
        
    
    y_pred_mod.append(np.array(y_pred_temp))
    y_test_mod.append(np.array(y_test_temp))
    

fig, ax = plt.subplots()
ax.scatter(y_test_mod[0], y_pred_mod[0])
ax.plot([y_test_mod[0].min(),y_test_mod[0].max()], [y_test_mod[0].min(), y_test_mod[0].max()], 'k--', lw=4)
ax.set_xlabel('Measured')
ax.set_ylabel('Predicted')
plt.show()