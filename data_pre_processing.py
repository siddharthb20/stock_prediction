# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 12:00:25 2021

@author: Siddharth
"""

import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np

def load_data():
    
    df1=pd.read_csv('dataset_target.csv')
    col1=df1.columns
    
    df2=pd.read_csv('dataset_target_2.csv')
    col2=df2.columns
    
    df3=pd.read_csv('Dataset_main.csv')
    col3=df3.columns
    
    df3.fillna(0, inplace=True)
    
    y = df3[['High', 'Low', 'Open', 'Close', 'Volume', 'Adj Close']]
    
    y_labels = y.drop(['Adj Close', 'Volume'], axis = 1)
    
    drop_columns = y.columns.tolist()
    drop_columns.append('Date')
    
    X=df3.drop(drop_columns,axis=1).values
    
    print(type(X))
    
    return X, y_labels

load_data()