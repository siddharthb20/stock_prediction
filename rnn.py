# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 16:07:08 2021

@author: Siddharth
"""

import datetime as dt
from sklearn import model_selection
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
from sklearn.metrics import accuracy_score
from tensorflow.keras.layers import BatchNormalization
import tensorflow as tf


df1 = pd.read_csv('dataset_target.csv')

df2 = pd.read_csv('dataset_target_2.csv')

df = df2.drop(['Date'], axis = 1)

df_train = df[:2200]

scaler = MinMaxScaler(feature_range = (0, 1))

target_df = df_train[['High', 'Low', 'Open', 'Close']]

train_set = df_train.values
target_set = target_df.values

training_set_scaled = scaler.fit_transform(train_set)
target_set_scaled = scaler.fit_transform(target_set)

X_train = []
y_train = []
for i in range(50, len(train_set)):
    X_train.append(training_set_scaled[i-50:i, :])
    y_train.append(target_set_scaled[i,:])
    
X_train, y_train = np.array(X_train), np.array(y_train)

# print(X_train.shape)

BATCH_SIZE = 32
EPOCHS = 20

def lstm_model():
    
    model = Sequential([LSTM(units = 64, return_sequences = True, input_shape = (X_train.shape[1], 9)),
                        Dropout(0.2),
                        BatchNormalization(),
                        LSTM(units = 64, return_sequences = True),
                        Dropout(0.1),
                        BatchNormalization(),
                        LSTM(units = 64),
                        Dropout(0.1),
                        BatchNormalization(),
                        Dense(units = 16, activation='tanh'),
                        BatchNormalization(),
                        Dense(units = 4, activation='tanh'),
                        ])
    
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy','mean_squared_error'])
    
    model.summary()
    
    return model

LSTM_mod = lstm_model() 

callback=tf.keras.callbacks.ModelCheckpoint(filepath='./RNN_model.h5',
                                           monitor='mean_squared_error',
                                           verbose=0,
                                           save_best_only=True,
                                           save_weights_only=False,
                                           mode='auto',
                                           save_freq='epoch')
print(y_train.shape)
LSTM_mod.fit(X_train, y_train, epochs = EPOCHS, batch_size = BATCH_SIZE, callbacks=[callback])

df_test = df[2200:]

df_target_test = df_test[['High','Low','Open','Close']]

target_set_test = df_target_test.values

test_set = df_test.values

test_set_scaled = scaler.fit_transform(test_set)
target_set_scaled = scaler.fit_transform(target_set_test)

X_test = []
y_test = []

for i in range(50,len(test_set)):
    X_test.append(test_set_scaled[i-50:i,:])
    y_test.append(target_set_scaled[i,:])
    
X_test, y_test = np.array(X_test), np.array(y_test)

predicted_stock_price = LSTM_mod.predict(X_test)

predicted_stock_price = scaler.inverse_transform(predicted_stock_price)

print(predicted_stock_price)

