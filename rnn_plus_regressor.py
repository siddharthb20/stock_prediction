# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 11:25:31 2021

@author: Siddharth
"""

import datetime as dt
from sklearn import model_selection
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
from sklearn.metrics import accuracy_score
from tensorflow.keras.layers import BatchNormalization
import tensorflow as tf
from rnn import lstm_model
import matplotlib.pyplot as plt


df1 = pd.read_csv("dataset_target_2.csv")
df2 = pd.read_csv("dataset_target.csv")
df3 = pd.read_csv("Dataset_main.csv")

df3.fillna(0, inplace = True)

scaler = MinMaxScaler(feature_range = (0, 1))

y_df=df3[['High', 'Low', 'Open', 'Close', 'Volume', 'Adj Close']]

col_y = y_df.columns

y_df_mod = y_df.drop(['Adj Close','Volume'], axis=1)

drop_col = col_y.to_list()

drop_col.append("Date")

X = df3.drop(drop_col, axis=1).values

saved_model_regressor = tf.keras.models.load_model('Regressor_model.h5')

regressor_prediction = saved_model_regressor(X)

print(type(regressor_prediction))
N, D = regressor_prediction.shape

print(N, D)

modified_y_pred = []

for i in range(0, 4):
    
    j = 0
    y_pred_temp=[]
   
    while(j < N):
        y_pred_temp.append(regressor_prediction[j][i])
        
        j += 1
    
    modified_y_pred.append(np.array(y_pred_temp))
    
print(modified_y_pred)  

Y_pred = pd.DataFrame(list(zip(modified_y_pred[0],modified_y_pred[1],modified_y_pred[2],modified_y_pred[3])),columns=['High_regress','Low_regress','Open_regress','Close_regress'])

print(Y_pred.head())
Y_pred.to_csv('Regressor_results.csv', index=False)

df_main = df1.drop(['Date'], axis=1)

main_set = df_main.values

X_test = []

for i in range(50,len(main_set)):
    X_test.append(main_set[i-50:i,:])
    
X_test = np.array(X_test)

saved_model_RNN = tf.keras.models.load_model('RNN_model.h5')
RNN_model = lstm_model()
RNN_pred = RNN_model.predict(X_test)

print(len(RNN_pred))

y_pred_mod = []

for i in range(0, 4):
    
    j=0
    y_pred_temp=[]
   
    
    while(j<len(RNN_pred)):
        y_pred_temp.append(RNN_pred[j][i])
        
        j+=1
        
    
    y_pred_mod.append(np.array(y_pred_temp))
    
Y_pred = pd.DataFrame(list(zip(y_pred_mod[0],y_pred_mod[1],y_pred_mod[2],y_pred_mod[3])),columns=['High_RNN','Low_RNN','Open_RNN','Close_RNN'])

Y_pred.to_csv('RNN_results.csv', index = False)

df1 = pd.read_csv('Regressor_results.csv')
df2 = pd.read_csv('RNN_results.csv')

df1 = df1[50:]
df1.to_csv('Modified_reg_results.csv', index = False)
df1 = pd.read_csv('Modified_reg_results.csv')

df=pd.concat([df1,df2],axis=1)

df1=pd.read_csv("dataset_target_2.csv")

target_high = []
target_low = []
i = 50
while i<len(df1):
    target_high.append(df1.iloc[i]['High'])
    target_low.append(df1.iloc[i]['Low'])
    i+=1
    
len(target_high)

df['Target_high'] = target_high
df['Target_low'] = target_low

df.to_csv('features.csv',index=False)

df_main=pd.read_csv('features.csv')

Xdf=df_main[['High_regress','Low_regress','Open_regress','Close_regress','High_RNN','Low_RNN','Open_RNN','Close_RNN']].values

df_main.hist(figsize = (35,35))
plt.show()

ydf = df_main[['Target_high','Target_low']].values

sc_2 = MinMaxScaler(feature_range = (0, 1))

X_train, X_test, y_train, y_test = train_test_split(Xdf, ydf, test_size=0.3)

X_train_sc = sc_2.fit_transform(X_train)

def model():
    
    mod=Sequential([
        Dense(32, kernel_initializer='normal',input_dim = 8, activation='relu'),
        Dense(64, kernel_initializer='normal',activation='relu'),
        Dense(128, kernel_initializer='normal',activation='relu'),
        Dense(2, kernel_initializer='normal',activation='linear')])
    
    mod.compile(loss='mean_absolute_error', optimizer='adam', metrics=['accuracy','mean_absolute_error'])
    mod.summary()
    
    return mod

model_ANN = model()
callback = tf.keras.callbacks.ModelCheckpoint(filepath='ANN_model.h5',
                                           monitor='mean_absolute_error',
                                           verbose=0,
                                           save_best_only=True,
                                           save_weights_only=False,
                                           mode='auto')

results = model_ANN.fit(X_train,y_train, epochs = 20, batch_size = 32, callbacks=[callback])

X_test_scaled = sc_2.transform(X_test)

y_pred = model_ANN.predict(X_test)

print(y_pred)
print(y_test)

y_pred_mod = []
y_test_mod = []

for i in range(0,2):
    j = 0
    y_pred_temp = []
    y_test_temp = []
    
    while(j < len(y_test)):
        y_pred_temp.append(y_pred[j][i])
        y_test_temp.append(y_test[j][i])
        j += 1
        
    
    y_pred_mod.append(np.array(y_pred_temp))
    y_test_mod.append(np.array(y_test_temp))
    
df_res = pd.DataFrame(list(zip(y_pred_mod[0],y_pred_mod[1],y_test_mod[0],y_test_mod[1])),columns=['Pred_high','Pred_low','Actual_high','Actual_low'])

df_res.to_csv('Results.csv',index=False)

df_res_2 = df_res[500:600]
#pred_price = scaler.inverse_transform(RNN_pred)

ax1=plt.subplot2grid((4,1), (0,0), rowspan=5, colspan=1)

ax1.plot(df_res_2.index, df_res_2['Pred_high'], label="Pred_high")
ax1.plot(df_res_2.index, df_res_2['Actual_high'], label="Actual_high")

plt.legend(loc="upper left")
plt.xticks(rotation=90)

plt.show()