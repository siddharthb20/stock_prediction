# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 02:41:33 2021

@author: Siddharth
"""

import pandas as pd
import numpy as np

def dataloader():
    
    df = pd.read_excel('M&MFIN.NS.xlsx', engine='openpyxl', index_col = 0)
    opening_price = np.array(df['Open']/1000000)
    closing_price = np.array(df['Close']/1000000)
    adj_closing = np.array(df['Adj Close']/1000000)
    #print(adj_closing)
    #print(df.columns)
    return df